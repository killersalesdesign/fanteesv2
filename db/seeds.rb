# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.find_by_email 'keenan@test.com'
if user.nil?
  user     = User.create(email: 'keenan@test.com', password: 'passw0rd', firstname: 'keenan', lastname: 'iban', role: 1)
end

Product.delete_all
ActiveRecord::Base.connection.execute("ALTER TABLE products AUTO_INCREMENT = 1")
product = Product.create(sales_goal: 20, end_campaign: 50.days.from_now, title: "Test title", description: "hello world", international_cutoff: 5, domestic_cutoff: 3, international_additional_price: 5, classification: 0, url: 'seed-test', campaign_status: 0, status: Product::STATUS[:approved], user_id: user.id, website_id: Website.first.id)

ProductType.delete_all
ActiveRecord::Base.connection.execute("ALTER TABLE product_types AUTO_INCREMENT = 1")
ProductType.create(product_id: product.id, category_id: Category.first.id, international_shipping: 5.0, domestic_shipping: 3.0, price: 29.99, profit: 3.99, print_colors: 3, front_image: File.new("/Users/arcknine/Downloads/front-tee-black.jpg"), back_image: File.new("/Users/arcknine/Downloads/back-tee-black.jpg"), color_id: 1)
ProductType.create(product_id: product.id, category_id: Category.first.id, international_shipping: 5.0, domestic_shipping: 3.0, price: 29.99, profit: 3.99, print_colors: 3, front_image: File.new("/Users/arcknine/Downloads/front-tee-white.jpg"), back_image: File.new("/Users/arcknine/Downloads/back-tee-white.jpg"), color_id: 2)
ProductType.create(product_id: product.id, category_id: Category.first.id, international_shipping: 5.0, domestic_shipping: 3.0, price: 29.99, profit: 3.99, print_colors: 3, front_image: File.new("/Users/arcknine/Downloads/front-tee.jpeg"), back_image: File.new("/Users/arcknine/Downloads/back-tee.jpeg"), color_id: 3)
ProductType.create(product_id: product.id, category_id: Category.first.id, international_shipping: 5.0, domestic_shipping: 3.0, price: 29.99, profit: 3.99, print_colors: 3, front_image: File.new("/Users/arcknine/Downloads/front-tee-red.jpeg"), back_image: File.new("/Users/arcknine/Downloads/back-tee-red.jpg"), color_id: 4)