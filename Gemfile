source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'
gem 'mysql2'
gem 'figaro'
gem 'devise'
gem 'rabl'
gem 'ledermann-rails-settings', :require => 'rails-settings'
gem 'paperclip', '~> 3.4.2'
gem 'aws-sdk', '< 2.0'
gem 'friendly_id'
gem 'groupdate'
gem 'mail_form'
gem 'net-ssh', '~> 2.9.1'
gem 'kaminari'
# gem 'meta_search'
gem 'rails_admin'
gem 'json'
gem 'sendgrid-api'
gem 'slim'
gem 'sinatra', :require => nil
gem 'slackistrano', require: false

# Assets
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'haml_coffee_assets'
gem 'execjs'
gem "fog", "~>1.20", require: "fog/aws/storage"
gem 'asset_sync'
gem 'gon'
gem 'eco'
gem 'haml-rails'
gem 'js-routes'
gem 'toastr-rails'
gem 'ng-rails-csrf'
gem 'non-stupid-digest-assets'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Worker
gem 'sidekiq'
gem 'sidetiq'

# Server
gem 'unicorn'
gem 'airbrake'

# Payment gateway
gem 'activemerchant'

group :production, :staging do
  gem 'newrelic_rpm'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'quiet_assets'
  gem 'thin'
  gem 'letter_opener'

  # Deployment
  gem 'capistrano', '~> 3.1'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-sidekiq'
  gem 'capistrano-unicorn-nginx'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  # gem 'capistrano-rbenv'
end

