# Fantees 2.0

## Requirements

- Ruby 2.1
- MySQL 5.6
- Redis
- ImageMagick 6.8

## AWS S3 Staging Buckets:

    FOG_DIRECTORY: teecommerce-fantees-staging
    AWS_ASSET_HOST: teecommerce-fantees-staging.s3.amazonaws.com
    AWS_PRODUCT_IMG_BUCKET: teecommerce-seller-staging
    AWS_PRODUCT_IMG_HOST: teecommerce-seller-staging.s3.amazonaws.com