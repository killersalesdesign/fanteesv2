require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get contact_us" do
    get :contact_us
    assert_response :success
  end

  test "should get send_contact_us" do
    get :send_contact_us
    assert_response :success
  end

  test "should get about_us" do
    get :about_us
    assert_response :success
  end

  test "should get faq" do
    get :faq
    assert_response :success
  end

  test "should get terms" do
    get :terms
    assert_response :success
  end

  test "should get privacy" do
    get :privacy
    assert_response :success
  end

  test "should get shipping" do
    get :shipping
    assert_response :success
  end

  test "should get returns_exchanges" do
    get :returns_exchanges
    assert_response :success
  end

  test "should get sizing" do
    get :sizing
    assert_response :success
  end

  test "should get processing" do
    get :processing
    assert_response :success
  end

  test "should get pinterest" do
    get :pinterest
    assert_response :success
  end

  test "should get win" do
    get :win
    assert_response :success
  end

end
