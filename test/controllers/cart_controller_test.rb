require 'test_helper'

class CartControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get creditcard" do
    get :creditcard
    assert_response :success
  end

end
