App.factory "Cart", [ "$resource", ($resource) ->
  Cart = $resource "/api/carts/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}

  return Cart
]

App.factory "Coupon", [ "$resource", ($resource) ->
  Coupon = $resource "/api/coupons/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}

  return Coupon
]

App.factory "UnfinishedOrder", [ "$resource", ($resource) ->
  UnfinishedOrder = $resource "/api/unfinished_orders/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}

  return UnfinishedOrder
]