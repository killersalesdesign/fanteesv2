@App.controller 'CartListCtrl', [ '$scope', "$http", "flashr", "Cart", "Coupon", ($scope, $http, flashr, Cart, Coupon) ->
  NProgress.start()
  $scope.coupon      = gon.coupon || {code: null}
  $scope.show_coupon = (if $scope.coupon.discount then true else false)

  Cart.query (items) ->
    $scope.items = items

    shipping_cost()
    total_cost items

    $scope.discount  = ($scope.coupon.discount * $scope.total) / 100 if $scope.coupon.discount

    NProgress.done()

  $scope.removeItem = (id) ->
    NProgress.start()
    Cart.delete id: id, (data) ->
      $scope.items = data.items

      shipping_cost()
      total_cost data.items

      $("#cart-num-main").text data.items.length

      NProgress.done()

  shipping_cost = ->
    $http.get(Routes.api_carts_shipping_cost_path()).success (data, status, headers, config) ->
      $scope.shipping = data.cost
      return

  total_cost = (items) ->
    total_quantity = 0
    $scope.total   = 0

    $.each items, (key, value) ->
      total_quantity += value.quantity
      $scope.total   += value.price * value.quantity

  $scope.toggleCoupon = ->
    $scope.show_coupon = (if $scope.show_coupon then false else true)

  $scope.applyCoupon = ->
    if $scope.coupon.code
      Coupon.get id: $scope.coupon.code, (coupon) ->
        unless coupon.code
          $scope.coupon   = {code: $scope.coupon.code}
          $scope.discount = 0
          $("#coupon-discount-form").addClass("form-error")
        else
          $scope.discount = (coupon.discount * $scope.total) / 100
          $scope.coupon   = coupon
          $("#coupon-discount-form").removeClass("form-error")
]

@App.controller 'CartCreditCardCtrl', [ '$scope', "$http", "flashr", "Cart", "Coupon", "COUNTRIES", "UnfinishedOrder", ($scope, $http, flashr, Cart, Coupon, COUNTRIES, UnfinishedOrder) ->
  NProgress.start()
  $scope.checkout    = gon.order
  $scope.coupon      = gon.coupon || {code: null}
  $scope.show_coupon = (if $scope.coupon.discount then true else false)

  Cart.query (items) ->
    $scope.items = items

    shipping_cost()
    total_cost items

    $scope.discount  = ($scope.coupon.discount * $scope.total) / 100 if $scope.coupon.discount

    NProgress.done()

  $scope.removeItem = (id) ->
    NProgress.start()
    Cart.delete id: id, (data) ->
      $scope.items = data.items

      $("#cart-num-main").text data.items.length

      computePayment data.items

      NProgress.done()

  shipping_cost = ->
    $http.get(Routes.api_carts_shipping_cost_path()).success (data, status, headers, config) ->
      $scope.shipping = data.cost
      return

  total_cost = (items) ->
    total_quantity = 0
    $scope.total   = 0

    $.each items, (key, value) ->
      total_quantity += value.quantity
      $scope.total   += value.price * value.quantity

  $scope.selectCountry = ->
    result = $.grep COUNTRIES, (e) ->
      e.name is $scope.checkout.country_name

    $scope.country = result[0] if result.length > 0
    $scope.checkout.country_code = $scope.country.iso

  $scope.toggleCoupon = ->
    $scope.show_coupon = (if $scope.show_coupon then false else true)

  $scope.fieldBlur = (e) ->
    $http.post Routes.api_unfinished_orders_path(), checkout_info: $scope.checkout
    .success (data, status, headers, config) ->
      console.log data
      return
    # console.log $scope.checkout
    # UnfinishedOrder

  $scope.applyCoupon = ->
    if $scope.coupon.code
      Coupon.get id: $scope.coupon.code, (coupon) ->
        unless coupon.code
          $scope.coupon   = {code: $scope.coupon.code}
          $scope.discount = 0
          $("#coupon-discount-form").addClass("form-error")
        else
          $scope.discount = (coupon.discount * $scope.total) / 100
          $scope.coupon   = coupon
          $("#coupon-discount-form").removeClass("form-error")

  $("#new_order").submit (event) ->

    has_erros = false
    $.each $scope.checkout, (key, value) ->
      if !$("input[name='order[#{key}]']").val() && (key != 'month' && key != 'year')
        has_erros = true
        $("input[name='order[#{key}]']").addClass "form-error"
      else
        $("input[name='order[#{key}]']").removeClass "form-error"

    unless has_erros
      # event.preventDefault()
      $(".place-order-now").attr("disabled", "disabled")
      # $(".place-order-now").val("Please Wait...")
      i = 0
      setInterval (->
        i = ++i % 4
        $(".place-order-now").val "Please Wait" + Array(i + 1).join('.')
        return
      ), 300
      true
    else
      false

  return
]