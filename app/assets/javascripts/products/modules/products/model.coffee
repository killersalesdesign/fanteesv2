App.factory "Product", [ "$resource", ($resource) ->
  Product = $resource "/api/products/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}

  return Product
]

App.factory "ProductType", [ "$resource", ($resource) ->
  ProductType = $resource "/api/products/:product_id/product_types/:id",
                        {id: "@id"}
                        {product_id: "@product_id"}
                        {update: {method: "PUT"}}

  return ProductType
]

App.factory "Color", [ "$resource", ($resource) ->
  Color = $resource "/api/products/:product_id/product_type_colors/:id",
                        {id: "@id"}
                        {product_id: "@product_id"}
                        {update: {method: "PUT"}}

  return Color
]
