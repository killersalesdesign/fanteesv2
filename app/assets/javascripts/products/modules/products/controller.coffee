@App.controller 'ProductShowCtrl', [ '$scope', "$http", "$location", "$modal", "flashr", "Product", "ProductType", "Color", "Cart", ($scope, $http, $location, $modal, flashr, Product, ProductType, Color, Cart) ->
  NProgress.start()

  slug = window.location.pathname.replace(/\//g, "")

  Product.get id: slug, (data) ->
    $scope.product           = data.product
    $scope.product.is_admin  = data.is_admin
    $scope.all_product_types = ProductType.query {product_id: data.product.id, show: 'all'}
    init_county(data.product.end_campaign)

    default_product_type  = data.default_product_type
    Color.query product_id: data.product.id, (product_types) ->
      product_types[0].class = "selected"
      $scope.colors  = product_types

      $scope.current_p_type_id  = default_product_type.id
      $scope.thumb_images       = default_product_type.images
      default_view              = (if default_product_type.default_view == 0 then 0 else 1)
      $scope.selected_thumb_img = default_product_type.images[default_view]
      $scope.selected_thumb_img.class = 'current'
      $scope.select_color product_types[0]
      NProgress.done()

    ProductType.query product_id: data.product.id, (types) ->
      $scope.product_types = types
      $.each $scope.product_types, (k, v) ->
        v.name = "#{v.name} - $#{v.price}"

      $scope.product_type = types[0]

      # fix for select2 dropdown
      setTimeout(->
        $('#big-image').elevateZoom()
        $(".select2-chosen").text $scope.product_type.name
      , 50)


  $scope.select_thumb_img = (image) ->
    $.each $scope.thumb_images, (key, value) ->
      value.class = ''

    $scope.selected_thumb_img       = image
    $scope.selected_thumb_img.class = 'current'



  $(document).on "click", ".thumbnail-holder img", ->
    setTimeout(->
      $('.zoomWindowContainer div').css("background-image","url(#{$scope.selected_thumb_img.original})")
    , 300)

  $scope.select_color = (color) ->
    $scope.current_p_type_id = color.id
    $.each $scope.colors, (key, value) ->
      value.class = ""

    color.class = "selected"

    NProgress.start()

    ProductType.get {product_id: $scope.product.id, id: color.id}, (data) ->
      $scope.thumb_images[0]    = data.front_images
      $scope.thumb_images[1]    = data.back_images
      default_view              = (if data.default_view == 0 then 0 else 1)
      $scope.selected_thumb_img = $scope.thumb_images[default_view]
      $scope.selected_thumb_img.class = 'current'
      NProgress.done()

  $scope.select_product_type = (e) ->
    NProgress.start()
    $scope.current_p_type_id = e.product_type.id
    # console.log e
    setTimeout(->
      $(".select2-chosen").text(e.product_type.name)
    , 50)

    Color.query {product_id: $scope.product.id, category_id: e.product_type.category_id}, (colors) ->
      colors[0].class = "selected"
      $scope.colors = colors

      ProductType.get {product_id: $scope.product.id, id: e.product_type.id}, (data) ->
        $scope.thumb_images[0]    = data.front_images
        $scope.thumb_images[1]    = data.back_images
        default_view              = (if data.default_view == 0 then 0 else 1)
        $scope.selected_thumb_img = $scope.thumb_images[default_view]
        $scope.selected_thumb_img.class = 'current'

        setTimeout(->
          $('.zoomWindowContainer div').css("background-image","url(#{$scope.selected_thumb_img.original})")

          NProgress.done()
        , 300)

  init_county = (date) ->
    newYear = new Date(date)
    $("#defaultCountdown").countdown until: newYear, padZeroes: true

    # county = $('#my-count-down').county
    #   endDateTime: new Date(date)
    #   reflection: false
    #   animation: 'scroll'
    #   theme: 'white'
    #   timezone: +10
    # destroy = null

  $scope.openModal = ->
    $modal.open
      template: JST["products/modules/products/modal"]
      backdrop: true
      windowClass: 'modal'
      controller: ["$scope", "$modalInstance", "$modal", "product", "product_type_id", "types", ($scope, $modalInstance, $modal, product, product_type_id, types) ->
        $scope.product = product
        $scope.type_id = product_type_id
        $scope.csrf    = $('meta[name=csrf-token]').attr('content')
        $scope.types   = types
        $scope.checkout_now = (if product.is_admin then false else true)
        $scope.cart = new Cart(items: [])
        cart_items  = []

        $scope.delivery_date =
          domestic:      moment(product.end_campaign).add(10, "days").format("MMM D, YYYY")
          international: moment(product.end_campaign).add(17, "days").format("MMM D, YYYY")

        default_type   = types[0]
        $.each types, (key, value) ->
          default_type = value if product_type_id == value.id

        # default_type   = types[0]
        # console.log default_type.sizes[0]
        $scope.addMore = [
          size: default_type.sizes[0]
          qty:  default_type.quantities[0]
          type: default_type
        ]

        $scope.addMoreStyle = ->
          default_type = $scope.types[0]
          $scope.addMore.push
            size: default_type.sizes[0]
            qty:  default_type.quantities[0]
            type: default_type

        $scope.removeItem = (index) ->
          $scope.addMore.splice(index, 1)

        $scope.cancel = ->
          $modalInstance.dismiss('cancel')

        $scope.cart_checkout = ->
          $scope.checkout_now = true

        $scope.selectType = (e, index) ->
          # console.log e.c, index
          $.each e.c.type.sizes, (key, value) ->
            if e.c.size.name is value.name
              $scope.addMore[index].size = value
            else
              $scope.addMore[index].size = e.c.type.sizes[0]

        $scope.add_to_cart = ->
          NProgress.start()
          toCart (cart) ->
            total_items = cart.items.length
            $modalInstance.dismiss('cancel')
            NProgress.done()
            $("#cart-num-main").text(total_items)

            $modal.open
              template: JST["products/modules/products/added_to_cart"]
              backdrop: true
              windowClass: 'modal'
              resolve:
                cart: ->
                  $scope.cart
              controller: ["$scope", "$modalInstance", "cart", "ProductType", ($scope, $modalInstance, cart, ProductType) ->
                item             = cart.items[0]
                $scope.show_back = false
                ProductType.get { product_id: item.product_id, id: item.product_type_id }, (type) ->
                  $scope.front_image = type.front_images.medium
                  $scope.back_image  = type.back_images.medium
              ]

        toCart = (callback) ->
          $.each $scope.addMore, (key, value) ->
            cart_items.push
              product_id: value.type.product_id
              quantity:   value.qty
              size_id:    value.size.id
              product_type_id: value.type.id
              price:      value.type.price

          $scope.cart.items = cart_items
          $scope.cart.$save (cart, putResponseHeaders) ->
            callback(cart)

      ]
      resolve:
        product: ->
          $scope.product

        product_type_id: ->
          $scope.current_p_type_id

        types: ->
          $scope.all_product_types
]