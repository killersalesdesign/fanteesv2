@App = angular.module('FanTees', [
  'ngResource'
  'ng-rails-csrf'
  'ui.bootstrap'
  'ui.select2'
  'geolocation'
  'flashr'
])

angular.module('FanTees').filter "niceDate", ->
  (date) ->
    moment(date).format("MMM Do YYYY")