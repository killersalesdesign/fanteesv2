$(document).ready ->
  $('.calculate').click ->
    fb = (if $('#fb-fans').val() then parseFloat($('#fb-fans').val()) else 0)
    tw = (if $('#tw-fans').val() then parseFloat($('#tw-fans').val()) else 0)
    yt = (if $('#yt-fans').val() then parseFloat($('#yt-fans').val()) else 0)
    ot = (if $('#ot-fans').val() then parseFloat($('#ot-fans').val()) else 0)

    sumfivetotal   = (fb + tw + yt + ot) * 0.05
    prod1fivetotal = parseFloat(sumfivetotal) * 0.0429
    prod2fivetotal = parseFloat(prod1fivetotal) * 12

    sumtentotal    = (fb + tw + yt + ot) * 0.10
    prod1tentotal  = parseFloat(sumtentotal) * 0.0429
    prod2tentotal  = parseFloat(prod1tentotal) * 12

    sumtwentytotal   = (fb + tw + yt + ot) * 0.20
    prod1twentytotal = parseFloat(sumtwentytotal) * 0.0429
    prod2twentytotal = parseFloat(prod1twentytotal) * 12

    $('.five-percent').text parseFloat(prod2fivetotal.toFixed(2))
    $('.ten-percent').text parseFloat(prod2tentotal.toFixed(2))
    $('.twenty-percent').text parseFloat(prod2twentytotal.toFixed(2))

  $(window).scroll ->
    $('.first-gif').attr('src','/assets/gif-1-animation.gif') if $(this).scrollTop() == 400

    $('.second-gif').attr('src','/assets/gif-2-animation.gif') if $(this).scrollTop() == 1340

    $('.third-gif').attr('src','/assets/gif-animation-3.gif') if $(this).scrollTop() == 1900