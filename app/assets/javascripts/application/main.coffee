$(document).ready ->
  $("#diagram-id-1").diagram
    size: "70"
    borderWidth: "6"
    bgFill: "#2F3D51"
    frFill: "#42BD9C"
    textSize: 23
    textColor: "#333"

  stickyNavTop  = $(".cart-btn").offset().top
  stickyNavLeft = $(".cart-btn").offset().left
  $(".cart-btn").css("left", stickyNavLeft+"px")
  stickyNav = ->
    scrollTop = $(window).scrollTop()
    if scrollTop > stickyNavTop
      $(".cart-btn").addClass "sticky"
    else
      $(".cart-btn").removeClass "sticky"
    return

  stickyNav()
  $(window).scroll ->
    stickyNav()

  $("body").css("visibility", "visible")


  # $("button.navbar-toggle").click ->
  #   setTimeout( ->
  #     if $("#bs-example-navbar-collapse-1").hasClass("in")
  #       $("#bs-example-navbar-collapse-1").removeClass("collapse")
  #   , 360)
