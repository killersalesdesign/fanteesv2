// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require lib/jquery.diagram
//= require lib/jquery.elevatezoom
//= require lib/bootstrap/bootstrap.min
//= require lib/html5shiv.min
//= require lib/respond.min
//= require lib/modernizr.custom
//= require lib/select2.min
//= require lib/nprogress
//= require lib/county
//= require lib/moment
//= require hamlcoffee
//= require js-routes
//= require lib/angular/angular
//= require lib/angular/angular-flashr
//= require lib/angular/angular-route
//= require lib/angular/angular-select2
//= require lib/angular/angular-geolocation
//= require lib/angular/angular-resource
//= require lib/bootstrap/ui-bootstrap-0.10.0
//= require lib/jquery.plugins
//= require lib/jquery.countdown
//= require ng-rails-csrf
//= require_tree ./application
//= require_tree ./products