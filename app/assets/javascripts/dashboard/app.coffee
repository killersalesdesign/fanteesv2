@App = angular.module('FanTees', [
  'ngResource'
  'ng-rails-csrf'
  'ui.bootstrap'
  'ui.select2'
  'geolocation'
  'flashr'
  'ngSanitize'
  'ngRoute'
])
.config [ '$routeProvider', '$locationProvider', '$httpProvider', ($routeProvider, $locationProvider, $httpProvider) ->
  $locationProvider.html5Mode
    enabled     : false
    requireBase : false

  $routeProvider
  .when "/",
    template: JST["dashboard/modules/home/index"]
    controller: "HomeCtrl"

  .when "/orders",
    template: JST["dashboard/modules/orders/index"]
    controller: "OrdersList"

  .when "/orders/:id",
    template: JST["dashboard/modules/orders/show"]
    controller: "OrderShow"

  .otherwise redirectTo: '/'
]
.config ["$httpProvider", (provider) ->
  provider.defaults.headers.common["X-CSRF-Token"] = $("meta[name=csrf-token]").attr("content")
]
