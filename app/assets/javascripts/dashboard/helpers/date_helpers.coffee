angular.module('FanTees').filter "fromNow", ->
  (date) ->
    moment(date).fromNow()

angular.module('FanTees').filter "niceDate", ->
  (date) ->
    moment(date).format("MMM Do YYYY")

angular.module('FanTees').filter "niceDate2", ->
  (date) ->
    moment(date).format("MMMM D, YYYY")

angular.module('FanTees').filter "niceDateTime", ->
  (date) ->
    moment(date).format("MMM DD, YYYY h:mm:ss a")