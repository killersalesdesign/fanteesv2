App.factory "OrderItem", [ "$resource", ($resource) ->
  OrderItem = $resource "/dashboard/orders/:order_id/order_items/:id",
                        {id: "@id"}
                        {order_id: "@order_id"}
                        {update: {method: "PUT"}}

  return OrderItem
]