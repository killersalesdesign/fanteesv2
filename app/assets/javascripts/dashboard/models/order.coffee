App.factory "Order", [ "$resource", ($resource) ->
  Order = $resource "#{Routes.dashboard_orders_path()}/:id",
                        {id: "@id"}
                        {update: {method: "PUT"}}

  Order::payment_type_in_word = ->
    return (if @payment_type == 0 then "PayPal" else "Credit Card")

  Order::order_status = ->
    switch @status
      when 1
        return 'Printing'
      when 2
        return 'Shipping'
      when 3
        return 'Refunded'
      else
        return 'Campaign / Pending'

  return Order
]