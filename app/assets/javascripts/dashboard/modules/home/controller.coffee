@App.controller 'HomeCtrl', ['$scope', '$modal', 'Order', 'COUNTRIES', ($scope,  $modal, Order, countries) ->
  $scope.orders    = Order.query(limit: 15)
  $scope.countries = countries

  $scope.openModal = (order) ->
    order.shipping_info = JSON.parse order.shipping_address

    $.each countries, (key, value) ->
      $scope.country = value if value.iso is order.shipping_info.Country

    $modal.open
      template: JST["dashboard/modules/orders/modal"]
      backdrop: true
      windowClass: 'modal-shipping'
      resolve:
        order: ->
          order
        countries: ->
          $scope.countries
        country: ->
          $scope.country
      controller: ["$scope", "$modalInstance", "flashr", "countries", "order", "country", ($scope, $modalInstance, flashr, countries, order, country) ->
        $scope.order     = order
        $scope.countries = countries
        $scope.country   = country

        console.log order
        $scope.cancel = ->
          $modalInstance.dismiss('cancel')

        $scope.submit = ->
          NProgress.start()
          $scope.order.shipping_address = JSON.stringify $scope.order.shipping_info
          $scope.order.$update (order, putResponseHeaders) ->
            flashr.now.success('Shipping Info updated!')
            $scope.order.shipping_info = JSON.parse order.shipping_address
            NProgress.done()

          $modalInstance.dismiss('cancel')

        $scope.selectCountry = (e) ->
          $scope.country = e.country
          $scope.order.shipping_info.CountryName = e.country.name
          $scope.order.shipping_info.Country     = e.country.iso
      ]
]