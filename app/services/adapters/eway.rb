module Adapters
  class Eway
    attr_accessor :context
    attr_accessor :details
    attr_accessor :current_user

    def initialize(context)
      @context  = context
      @subtotal = 0
      @total    = 0
      @quantity = 0
      @shipping = context.options[:shipping]
      @coupon   = Coupon.find_by code: context.options[:coupon] || nil
      @items    = compute_items
      @errors   = context.errors
      @password = nil
    end

    def verify
      @order   = Order.new
      @user    = User.find_or_initialize_by email: @context.options[:order_info]["email"]
      response = purchase!

      Rails.logger.debug("TRANSACTION ID: #{response.inspect}")
      if response.success?
        @order.transaction_id   = response.params["ewaytrxnnumber"]
        @order.shipping_address = create_shipping_address

        initialize_or_create_user
      else
        @errors[:status] = 'Error'
        @errors[:messages] << response.message
      end

      return @user, @order
    end

    def success?
      @errors[:status] != 'Error'
    end

    private

    def purchase!
      info = @context.options[:order_info]
      credit_card = ActiveMerchant::Billing::CreditCard.new(
        :number     => info[:cc_number],
        :month      => info[:month].to_i,
        :year       => info[:year].to_i,
        :first_name => info[:first_name],
        :last_name  => info[:last_name],
        :verification_value => info[:cvn]
      )

      eway_checkout_options = {
        :subtotal => (@subtotal * 100).round,
        :shipping => (@shipping * 100).round,
        :handling => 0,
        :tax      => 0,
        :order_id => @context.options[:order_code],
        :email    => info[:email],
        :address => {
          :address1 => info[:address],
          :city     => info[:city],
          :state    => info[:state],
          :country  => info[:country],
          :zip      => info[:zip]
        },
        :shipping_address => {
          :address1 => info[:address],
          :city     => info[:city],
          :state    => info[:state],
          :country  => info[:country],
          :zip      => info[:zip]
        },
        :description => @context.options[:order_code]
      }

      gateway  = ActiveMerchant::Billing::EwayGateway.new(:login => ENV['EWAY_COSTUMER_ID'])
      response = gateway.purchase(@total.round, credit_card, eway_checkout_options)

      response
    end

    def initialize_or_create_user
      if @user.id.nil?
        @password                   = Devise.friendly_token[0,8]
        @user.firstname             = @context.options[:order_info]["first_name"]
        @user.lastname              = @context.options[:order_info]["last_name"]
        @user.password              = @password
        @user.password_confirmation = @password
        @user.role                  = User::ROLE[:buyer]
      end

      if @user.save
        create_order
      else
        # Create an error logger here
        @errors[:status] = "Error"
        @errors[:messages] << "Unable to create user!"
      end
    end

    def create_order
      @order.website_id    = @context.options[:website_id]
      @order.shipping_cost = @context.options[:shipping]
      @order.ip_address    = @context.options[:ip]
      @order.order_code    = @context.options[:order_code]
      @order.total_qty     = @quantity
      @order.total_amount  = @subtotal
      @order.status        = Order::STATUS[:pending]
      @order.payment_type  = Order::PAYMENT[:creditcard]
      @order.coupon_id     = @coupon.id unless @coupon.nil?
      @order.buyer_id      = @user.id
      @order.seller_id     = @items.first.product.user.id

      if @order.save
        items = []
        @items.each do |item|
          items << {
            quantity:        item.quantity,
            amount:          item.price,
            product_id:      item.product_id,
            product_type_id: item.product_type_id,
            category_id:     item.product_type.category.id,
            size_id:         item.size_id,
            color_id:        item.product_type.color.id,
            order_id:        @order.id
          }
        end

        OrderItem.create items
        @coupon.increment!(:usage, 1) unless @coupon.nil?
        @order.send_mail_on_success_order @password
      else
        # Create an error logger here
        @errors[:status] = "Error"
        @errors[:messages] << "Unable to create order!"
      end
    end

    def compute_items
      @context.items.each do |item|
        # insert coupon code here
        price = @coupon.nil? ? item.price : item.price - ((@coupon.discount.to_f * item.price.to_f) / 100)
        @quantity += item.quantity
        @subtotal += price * item.quantity
      end

      @subtotal = @subtotal
      @total    = (@subtotal * 100) + (@shipping * 100)

      @context.items
    end

    def create_shipping_address
      order_info = @context.options[:order_info]
      shipping_address = {
        Name: "#{order_info[:first_name]} #{order_info[:last_name]}",
        Street1: order_info[:address],
        Street2: "",
        CityName: order_info[:city],
        StateOrProvince: order_info[:state],
        Country: order_info[:country_code],
        CountryName: order_info[:country_name],
        Phone: order_info[:phone],
        PostalCode: order_info[:zip]
      }.to_json

      shipping_address
    end
  end
end