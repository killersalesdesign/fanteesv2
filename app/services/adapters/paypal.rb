module Adapters
  class Paypal
    attr_accessor :context
    attr_accessor :details

    def initialize(context)
      @context  = context
      @subtotal = 0
      @total    = 0
      @shipping = (context.options[:shipping] * 100).round
      @items    = context.items
      @quantity = 0
      @coupon   = Coupon.find_by code: context.options[:coupon] || nil
      @pp_items = generate_paypal_items
      @errors   = context.errors
      @password = nil
    end

    def redirect_url(options)
      options  = options.reverse_merge checkout_options
      response = EXPRESS_GATEWAY.setup_purchase(@total.round, options)

      if response.success?
        EXPRESS_GATEWAY.redirect_url_for response.token
      else
        @errors[:status] = 'Error'
        @errors[:messages] << response.message
      end
    end

    def verify
      details  = EXPRESS_GATEWAY.details_for @context.options[:token]
      response = purchase!

      if response.success?
        @order = Order.new
        @order.transaction_id   = response.params["transaction_id"]
        @order.shipping_address = details.params["PaymentDetails"]["ShipToAddress"].to_json

        initialize_or_create_user details

        return @user, @order
      else
        @errors[:status] = 'Error'
        @errors[:messages] << response.message

        return false
      end
    end

    def success?
      @errors[:status] != 'Error'
    end

    private

    def purchase!
      options = {
        :ip       => @context.options[:ip],
        :token    => @context.options[:token],
        :payer_id => @context.options[:payer_id]
      }

      options  = options.reverse_merge(checkout_options)
      response = EXPRESS_GATEWAY.purchase(@total.round, options)

      response
    end

    def checkout_options
      {
        :subtotal => @subtotal,
        :shipping => @shipping,
        :handling => 0,
        :tax      => 0,
        :items    => @pp_items,
        :currency => 'AUD'
      }
    end

    def generate_paypal_items
      items = []
      @items.each do |item|
        # insert coupon code here
        price = @coupon.nil? ? item.price : item.price - ((@coupon.discount.to_f * item.price.to_f) / 100)

        Rails.logger.debug("PRICE: #{price}   |   item.price: #{item.price}")
        items << {
          name:        item.product.title,
          quantity:    item.quantity,
          description: "#{item.size.name} #{item.product_type.category.name} - #{item.product_type.color.name}",
          amount:      (price * 100).round
        }

        @subtotal += price * item.quantity
        @quantity += item.quantity
      end

      @subtotal = (@subtotal * 100).round
      @total    = @subtotal + @shipping

      items
    end

    def initialize_or_create_user(details)
      @user = User.find_or_initialize_by(email: details.params["payer"])

      if @user.id.nil?
        @password                   = Devise.friendly_token[0,8]
        @user.firstname             = details.params["first_name"]
        @user.lastname              = details.params["last_name"]
        @user.password              = @password
        @user.password_confirmation = @password
        @user.role                  = User::ROLE[:buyer]
      end

      if @user.save
        create_order
      else
        # Create an error logger here
        @errors[:status] = "Error"
        @errors[:messages] << "Unable to create user!"
      end
    end

    def create_order
      @order.website_id    = @context.options[:website_id]
      @order.payer_id      = @context.options[:payer_id]
      @order.paypal_token  = @context.options[:token]
      @order.shipping_cost = @context.options[:shipping]
      @order.ip_address    = @context.options[:ip]
      @order.order_code    = @context.options[:order_code]
      @order.total_qty     = @quantity
      @order.total_amount  = (@subtotal / 100).to_f
      @order.status        = Order::STATUS[:pending]
      @order.payment_type  = Order::PAYMENT[:paypal]
      @order.coupon_id     = @coupon.id unless @coupon.nil?
      @order.buyer_id      = @user.id
      @order.seller_id     = @items.first.product.user.id

      if @order.save
        items = []
        @items.each do |item|
          items << {
            quantity:        item.quantity,
            amount:          item.price,
            product_id:      item.product_id,
            product_type_id: item.product_type_id,
            category_id:     item.product_type.category.id,
            size_id:         item.size_id,
            color_id:        item.product_type.color.id,
            order_id:        @order.id
          }
        end

        OrderItem.create items
        @coupon.increment!(:usage, 1) unless @coupon.nil?
        @order.send_mail_on_success_order @password
      else
        # Create an error logger here
        @errors[:status] = "Error"
        @errors[:messages] << "Unable to create order!"
      end
    end
  end
end