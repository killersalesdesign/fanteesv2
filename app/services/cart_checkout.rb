class CartCheckout
  def initialize(order)
    @order     = order
    @errors    = []
    @items     = []
    @subtotal  = 0
    @shipping  = 0
    @total_qty = 0
    @password  = nil
  end

  def initialize_items(items)
    domestic = ENV['APP_NAME'] == 'FanTees' ? 'AU' : 'NZ' # check if domestic shipping is AU/NZ
    country  = 'AU' # temp country checker
    cutoff   = 0
    @items   = items
    @paypal_items = []

    @items.each do |i|
      product      = Product.find i["product_id"]
      product_type = ProductType.find i["product_type_id"]
      size         = Size.find i["size_id"]
      @paypal_items << {
        name:        product.title,
        quantity:    i["quantity"],
        description: "#{size.name} #{product_type.category.name} - #{product_type.color.name}",
        amount:      (product_type.price * 100).round
      }

      # check if buyers' location is domestic or not
      if country == domestic
        @shipping = @shipping > product_type.domestic_shipping ? @shipping : product_type.domestic_shipping * 100
        cutoff    = product.domestic_cutoff
      else
        @shipping = @shipping > product_type.international_shipping ? @shipping : product_type.international_shipping * 100
        cutoff    = product.international_cutoff
      end

      @total_qty += i["quantity"].to_i
      @subtotal  += (product_type.price * i["quantity"].to_f) * 100

      @shipping = 0 if @total_qty >= cutoff
      # @shipping = @shipping.to_f * 100
      # @subtotal = @subtotal.to_f * 100
      @total    = @shipping + @subtotal
    end

    # update order
    @order.total_qty    = @total_qty
    @order.total_amount = @total.to_f / 100

    @items
  end

  def paypal_purchase
    options    = {
      :ip       => @order.ip_address,
      :token    => @order.paypal_token,
      :payer_id => @order.payer_id
    }
    options = options.reverse_merge(paypal_checkout_options)
    response = EXPRESS_GATEWAY.purchase(@total.round, options)

    response
  end

  def eway_purchase(params)
    credit_card = ActiveMerchant::Billing::CreditCard.new(
      :number     => params[:cc_number],
      :month      => params[:month].to_i,
      :year       => params[:year].to_i,
      :first_name => params[:first_name],
      :last_name  => params[:last_name],
      :verification_value => params[:cvn]
    )

    eway_checkout_options = {
      :subtotal => @subtotal * 100,
      :shipping => @shipping * 100,
      :handling => 0,
      :tax      => 0,
      :order_id => @order.order_code,
      :email    => params[:email],
      :address => {
        :address1 => params[:address],
        :city     => params[:city],
        :state    => params[:state],
        :country  => params[:country],
        :zip      => params[:zip]
      },
      :shipping_address => {
        :address1 => params[:address],
        :city     => params[:city],
        :state    => params[:state],
        :country  => params[:country],
        :zip      => params[:zip]
      },
      :description => @order.order_code
    }

    gateway   = ActiveMerchant::Billing::EwayGateway.new(:login => ENV['EWAY_COSTUMER_ID'])
    response  = gateway.purchase(@total.to_i, credit_card, eway_checkout_options)

    response
  end

  def create_order(params = {})
    if initialize_and_save_user(params)

      if @order.payment_type == Order::PAYMENT[:paypal]
        response = paypal_purchase
      else
        response = eway_purchase(params)
      end

      if response.success?
        @order.transaction_id = response.params["transaction_id"]

        product          = Product.find @items.first["product_id"]
        @order.seller_id = product.user.id
        @order.buyer_id  = @user.id

        if @order.save
          @items.each do |item|
            product_type = ProductType.find item["product_type_id"]
            params_order_items = {
              quantity:        item["quantity"],
              amount:          product_type.price,
              product_id:      item["product_id"],
              product_type_id: item["product_type_id"],
              category_id:     product_type.category.id,
              size_id:         item["size_id"],
              color_id:        product_type.color.id,
              order_id:        @order.id
            }
            order_item = OrderItem.new params_order_items
            order_item.save
          end

          @order.send_mail_on_success_order @password
        end

        return true
      else
        @errors << response.message
        return false
      end

    end
  end

  def initialize_and_save_user(params)
    if @order.payment_type == Order::PAYMENT[:paypal]
      details  = EXPRESS_GATEWAY.details_for @order.paypal_token
      @user    = User.find_or_initialize_by(email: details.params["payer"])

      if @user.id.nil?
        @user.firstname = details.params["first_name"]
        @user.lastname  = details.params["last_name"]
      end

      @order.shipping_address = details.params["PaymentDetails"]["ShipToAddress"].to_json
    else
      @user = User.find_or_initialize_by email: params[:email]

      if @user.id.nil?
        @user.firstname = params[:first_name]
        @user.lastname  = params[:last_name]
      end

      @order.shipping_address = {
        Name: "#{params[:first_name]} #{params[:last_name]}",
        Street1: params[:address],
        Street2: "",
        CityName: params[:city],
        StateOrProvince: params[:state],
        Country: params[:country_code],
        CountryName: params[:country_name],
        Phone: params[:phone],
        PostalCode: params[:zip]
      }.to_json

    end

    if @user.id.nil?
      @password                   = Devise.friendly_token[0,8]
      @user.password              = @password
      @user.password_confirmation = @password
    end

    @user.role = User::ROLE[:buyer] if @user.role.nil?

    if @user.save
      puts "TRUE"
      true
    else
      puts "FALSE"
      false
    end
  end

  def paypal_checkout_url(options)
    options  = options.reverse_merge(paypal_checkout_options)
    response = EXPRESS_GATEWAY.setup_purchase(@total.round, options)

    if @errors.empty?
      EXPRESS_GATEWAY.redirect_url_for(response.token)
    else
      @errors
    end
  end

  def errors
    @errors
  end

  def get_user
    @user
  end

  private

  def paypal_checkout_options
    {
      :subtotal          => @subtotal.round,
      :shipping          => @shipping.round,
      :handling          => 0,
      :tax               => 0,
      :items             => @paypal_items,
      :currency          => 'AUD',
    }
  end

end