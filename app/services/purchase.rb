require "forwardable"

class Purchase
  extend Forwardable

  attr_accessor :vendor, :adapter, :errors, :options, :items

  def_delegators :adapter,
  :verify,
  :success?,
  :status,
  :coupon,
  :shipping_cost,
  :price,
  :redirect_url

  def initialize(adapter, items, options={})
    @vendor      = adapter
    @items       = items
    @options     = options
    @errors      = {status: "Success", messages: []}
    self.adapter = adapter
  end

  def adapter=(adapter_name)
    @adapter = Adapters.const_get("#{adapter_name.to_s.classify}").new(self)
  end
end