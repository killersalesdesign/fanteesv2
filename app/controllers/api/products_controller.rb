class Api::ProductsController < ApplicationController
  def index
    product_type = ProductType.find params[:product_type_id]
    images = [
      {
        thumb:  product_type.front_image(:thumb),
        medium: product_type.front_image(:medium),
        original: product_type.front_image(:original)
      },
      {
        thumb:  product_type.back_image(:thumb),
        medium: product_type.back_image(:medium),
        original: product_type.back_image(:original)
      }
    ]
    render :json => images
  end

  def show
    product      = @website.products.find_by_slug params[:id]
    unless product.nil?
      product_type = product.product_types.first
      default_type = {
        id: product_type.id,
        default_view: product_type.default_view,
        images: [
          {
            thumb:  product_type.front_image(:thumb),
            medium: product_type.front_image(:medium),
            original: product_type.front_image(:original)
          },
          {
            thumb:  product_type.back_image(:thumb),
            medium: product_type.back_image(:medium),
            original: product_type.back_image(:original)
          }
        ]
      }
      render :json => {product: product, default_product_type: default_type, is_admin: product.user.is_admin?}
    else
      render :json => nil
    end
  end
end
