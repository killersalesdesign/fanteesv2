class Api::ProductTypesController < ApplicationController
  def index
    begin
      @product_types = Product.find(params[:product_id]).product_types.order("id ASC")
      @product_types = @product_types.group('category_id') unless params[:show]
    rescue
      @product_types = []
    end
  end

  def show
    @product_type = ProductType.find params[:id]
  end
end
