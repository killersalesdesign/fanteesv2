class Api::CartsController < ApplicationController
  def index

  end

  def show
    @item = Cart.find params[:id]
  end

  def create
    # checks if previous items are from admin if not will delete all and insert new items
    if session[:cart]["is_admin"] == false
      session[:cart]['is_admin'] = true
      cart = Cart.where tracking_code: session[:cart]['tracking_code']
      cart.delete_all
    end

    cart = Cart.create_cart_items params[:items], current_user, session[:cart]['tracking_code']

    render :json => {items: cart}
  end

  def destroy
    cart = Cart.find(params[:id])
    cart.delete

    @cart = Cart.where tracking_code: session[:cart]['tracking_code']
    items = []
    @cart.all.each do |item|
      items << {
        id:              item.id,
        quantity:        item.quantity,
        size:            item.size_id,
        price:           item.price,
        image:           item.product_type.front_image(:thumb),
        product_title:   item.product.title,
        size_name:       item.size.name,
        product_type_id: item.product_type_id,
        delivery_date:   item.product.end_campaign + 14.days,
        type_name:       item.product_type.name_and_color
      }
    end

    render :json => {items: items}
  end

  def shipping_cost
    render :json => {cost: Shipping.compute_shipping_cost('cart', session[:cart]['tracking_code'])}
  end

end
