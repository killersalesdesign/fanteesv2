class Api::CouponsController < ApplicationController
  def show
    coupon = Coupon.find_by code: params[:id]

    unless coupon.nil?
      session[:cart]['coupon'] = coupon.code
    else
      session[:cart]['coupon'] = nil
    end

    render :json => coupon
  end
end
