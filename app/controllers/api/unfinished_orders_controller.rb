class Api::UnfinishedOrdersController < ApplicationController
  def create
    @uo = UnfinishedOrder.find_or_initialize_by tracking_code: session[:cart]['tracking_code']
    @uo.website_id    = @website.id
    @uo.active        = true
    @uo.checkout_info = params[:checkout_info].to_s
    @uo.worker_ids    = initialize_workers
    render :json => @uo.save
  end

  # private

  def initialize_workers
    workers = UnfinishedOrder::WORKERS
    ids     = @uo.worker_ids || []

    if @uo.worker_ids.nil?
      workers.each do |w|
        puts "creating worker for #{w[:mail]}"
        ids << UnfinishedOrderWorker.perform_in(w[:duration], w[:mail], session[:cart]['tracking_code'])
      end
    end

    return ids.to_s
  end
end
