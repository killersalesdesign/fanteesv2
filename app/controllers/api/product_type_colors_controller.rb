class Api::ProductTypeColorsController < ApplicationController
  def index
    product      = Product.find params[:product_id]
    unless product.nil?
      if params[:category_id].blank?
        category_id = product.product_types.first.category_id
      else
        category_id = params[:category_id]
      end
      product_types = product.product_types.where(:category_id => category_id)

      colors       = []
      product_types.each do |t|
        colors << {id: t.id, category_id: t.category_id, name: t.color.name, hex: t.color.hex} if t.color.active
      end
      render :json => colors
    else
      render :json => []
    end
  end
end
