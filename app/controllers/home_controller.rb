class HomeController < ApplicationController
  def index
    @keyword  = params[:search] ? params[:search] : nil
    @products = @website.products.search(@keyword, Product::CLASSIFICATION[:regular]).page(params[:page]).per(16)

    @group1 = []
    @group2 = []
    @products.each_with_index do |p, index|
      if index < 8
        @group1 << p
      else
        @group2 << p
      end
    end
  end
  # end of index

end
