class Dashboard::OrdersController < Dashboard::BaseController
  def index
    @orders = current_user.made_orders.order("id desc")
    @orders = @orders.limit(params[:limit]) unless params[:limit].blank?
  end

  def show
    @order = Order.find_by order_code: params[:id]
  end

  def update
    @order = Order.find params[:id]

    @order.update order_params
  end

  private

  def order_params
    params.require(:order).permit(:shipping_address)
  end
end
