class Dashboard::OrderItemsController < Dashboard::BaseController
  def index
    order  = Order.find params[:order_id]
    @items = order.order_items
  end

  def show
  end
end
