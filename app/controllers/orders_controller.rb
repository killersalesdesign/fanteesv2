class OrdersController < ApplicationController
	def show
		@order = Order.find_by order_code: params[:id]

		unless @order.nil?
			@fb_conversion_ids 	= []
			@fb_custom_codes 		= []
			@ga_codes 					= []

			@order.order_items.each do |item|
				begin
					if item.product.fb_conversion_code.blank?
						user_conversion_code = item.product.user.fb_conversion_code
						unless user_conversion_code.nil?
							unless @fb_conversion_ids.include? user_conversion_code
								@fb_conversion_ids << user_conversion_code
							end
						end
					else
						@fb_conversion_ids << item.product.fb_conversion_code
					end

					user_fb_custom = item.product.user.fb_custom_audience_code
					unless user_fb_custom.nil?
						unless @fb_custom_codes.include? user_fb_custom
							@fb_custom_codes << user_fb_custom
						end
					end

					user_ga_code = item.product.user.google_analytics
					unless user_ga_code.nil?
						unless @ga_codes.include? user_ga_code
							@ga_codes << user_ga_code
						end
					end
				rescue
					redirect_to root_path
				end
			end
		else
			redirect_to "https://campaigns.fantees.com.au/orders/#{params[:id]}"
		end

	end
end
