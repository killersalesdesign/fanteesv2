class MailTrackerController < ApplicationController
	def render_pixel
		# add count for `open` action in email tracker
		MailTracker.increment_count("open", params[:time_frame])
		send_file "app/assets/images/checkout-comment-arrow-down.png", :type => 'image/png', :disposition => 'inline'
	end
end