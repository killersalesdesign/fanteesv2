class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :get_total_shipped, :get_website, :init_cart

  def get_website
    @website = Website.find_by_name(ENV['APP_NAME'])
  end

  def get_total_shipped
    @shipped = OrderItem.sum(:quantity) || 0

    @shipped += 25793
  end

  def init_cart
    session[:cart] ||= {tracking_code: Cart.create_tracking_code, coupon: nil}

    begin
      @cart = Cart.where tracking_code: session[:cart]['tracking_code']

      @cart.each do |item|
        if item.product.status == Product::STATUS[:deactivated]
          begin
            item.delete
          rescue
            puts 'cannot delete cart item'
          end
        end
      end
    rescue
    end
  end
end
