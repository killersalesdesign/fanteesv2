class CartController < ApplicationController
  layout 'products'

  def index
    @order = Order.new
    gon.coupon  = Coupon.find_by code: session[:cart]['coupon']
  end

  def show
    session[:cart]['tracking_code'] = params[:id]

    @cart  = Cart.where tracking_code: params[:id]
    @uo    = UnfinishedOrder.find_by tracking_code: params[:id]
    @order = Order.new

    if @cart.empty?
      session[:cart]['tracking_code'] = Cart.create_tracking_code
      redirect_to '/500.html'
    else
      if @uo
        session[:rebuild] = true

        # add mail tracker for email clicks
        if params[:time_frame]
          time_frame = ""
          case params[:time_frame]
          when "30m"
            time_frame = "30-minutes"
          when "1d"
            time_frame = "1-day"
          when "10d"
            time_frame = "10-days"
          end

          MailTracker.increment_count("click", time_frame)
          session[:rebuild_time_frame] = time_frame
        end

        gon.coupon = Coupon.find_by code: session[:cart]['coupon']
        begin
          gon.order  = eval(@uo.checkout_info)
        rescue
          gon.order  = order_fields
        end

        # render :json => gon.order
        render 'creditcard'
      end
    end
  end

  def creditcard
    @order = Order.new
    if params[:order]
      gon.order  = params[:order]
      gon.coupon = Coupon.find_by code: session[:cart]['coupon']
    else
      gon.coupon = Coupon.find_by code: session[:cart]['coupon']
      gon.order  = order_fields
    end
  end

  def checkout
    # add_to_cart if params[:items]
    if params[:items] && params[:payment] == 'creditcard'
      add_to_cart
      creditcard_checkout
    else
      paypal_checkout
    end
  end

  private

  def paypal_checkout
    add_to_cart if params[:items]
    shipping = Shipping.compute_shipping_cost('cart', session[:cart]['tracking_code'])
    paypal   = Purchase.new 'paypal', @cart, {shipping: shipping, coupon: session[:cart]['coupon']}
    @cart    = Cart.where tracking_code: session[:cart]['tracking_code']

    begin
      pid      = @cart.last.product_id
    rescue
      redirect_to root_path
    end

    product  = Product.find pid

    express_checkout_url = paypal.redirect_url(
      :ip                => request.remote_ip,
      :return_url        => checkout_paypal_url(session[:cart]['tracking_code']),
      :cancel_return_url => "#{root_url}#{product.slug}"
    )

    if paypal.errors[:status] != 'Error'
      redirect_to express_checkout_url
    else
      render :json => paypal.errors
    end
  end

  def creditcard_checkout
    redirect_to creditcard_cart_index_path
  end

  def add_to_cart
    session[:cart][:is_admin] = false

    cart = Cart.where tracking_code: session[:cart]['tracking_code']
    cart.delete_all

    @cart = Cart.create_cart_items params[:items], current_user, session[:cart]['tracking_code']
  end

  def order_fields
    {
      :email      => "",
      :first_name => "",
      :last_name  => "",
      :phone      => "",
      :address    => "",
      :city       => "",
      :phone      => "",
      :zip        => "",
      :country_code => @website.name == "FanTees-NZ" ? "NZ" : "AU",
      :country_name => @website.name == "FanTees-NZ" ? "New Zealand" : "Australia",
      :state      => "",
      :cc_number  => "",
      :month      => 1.month.from_now.strftime("%m").to_i,
      :year       => 1.month.from_now.strftime("%Y").to_i,
      :cvn        => ""
    }
  end

end
