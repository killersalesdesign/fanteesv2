class PagesController < ApplicationController
  layout "pinterest", :only => :pinterest
  layout "heres_how", :only => :heres_how

  def contact_us
    @contact = ContactUs.new
  end

  def send_contact_us
    contact = ContactUs.new(params[:contact_us])
    if contact.valid? && !contact.spam?
      contact.deliver
      respond_to do |format|
        format.html { redirect_to "/contact-us?er=0", :flash => { :notice => "Thanks for getting in contact! We'll be in touch with you in the next 24-48 hours." } }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to "/contact-us?er=1", :flash => { :error => contact.errors.full_messages } }
        format.json { head :no_content }
      end
    end
  end

  def about_us
  end

  def faq
  end

  def terms
  end

  def privacy
  end

  def shipping
  end

  def returns_exchanges
  end

  def sizing
  end

  def processing
  end

  def pinterest
  end

  def win
  end

  def user_agreement
  end

  def heres_how

  end
end
