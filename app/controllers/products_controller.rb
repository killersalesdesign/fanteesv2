class ProductsController < ApplicationController
  def show
    @product = Product.find_by slug: params[:slug]
    # if @product.nil? or @product.status != Product::STATUS[:approved]
    if @product.nil?
      if params[:slug] == 'nz-scottish' || params[:slug] == 'shaker'
        redirect_to "https://fantees.com.au/#{params[:slug]}"
      else
        respond_to do |format|
          format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
          format.xml  { head :not_found }
          format.any  { head :not_found }
        end
      end
    else
      @sales = @product.total_sold

      # check if product is relaunch from previous campaign
      # get the quantity sold for the last campaign and add 10
      unless @product.prev_campaign_sold.nil?
        @sales = @sales + @product.prev_campaign_sold
      end

      # @product_fb_conversion_code = []
      # unless @product.fb_conversion_code.nil?
      #   @product_fb_conversion_code = [@product.fb_conversion_code]
      # end
      @fb_custom_audience_code = @product.user.fb_custom_audience_code

      # @fb_custom_code = @product.user.fb_custom_audience_code
      # @fb_custom_code = @product.fb_conversion_code unless @product.fb_conversion_code.blank?
      if @product.user.google_analytics.nil?
        @ga_code      = []
      else
        @ga_code 			= [@product.user.google_analytics]
      end
    end
  end
end
