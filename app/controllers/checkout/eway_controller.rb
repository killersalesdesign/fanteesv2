class Checkout::EwayController < ApplicationController
  layout 'products'

  def create
    @cart       = Cart.where tracking_code: session[:cart]['tracking_code']
    eway        = Purchase.new 'eway', @cart, checkout_options
    user, order = eway.verify

    begin
      @cart.user_id = user.id
      @cart.save
    rescue
    end

    if eway.success?
      # delete all cart items
      @cart.delete_all
      @uo = UnfinishedOrder.find_by tracking_code: session[:cart]['tracking_code']

      @uo.destroy_workers

      if session[:rebuild]
        # set rebuild date
        @uo.recovered = true
        @uo.recovered_date = Time.now
        @uo.save!

        if session[:rebuild_time_frame]
          # record rejoin in mail tracker
          MailTracker.increment_count("rejoins", session[:rebuild_time_frame])
        end
      else
        @uo.destroy_order
      end

      # stats worker
      UpdateStatisticWorker.perform_async(order.id)

      session.delete(:rebuild_time_frame)
      session.delete(:rebuild)

      # set coupon back to nil
      session[:cart]['coupon'] = nil
      session[:cart]['tracking_code'] = Cart.create_tracking_code

      # sign in user
      sign_in(:user, user)

      redirect_to order_path(order.order_code)
    else
      @order     = Order.new
      gon.order  = params[:order]
      gon.coupon = Coupon.find_by code: session[:cart]['coupon']

      @errors = eway.errors
      render 'cart/creditcard'
    end
  end

  private

  def checkout_options
    shipping = Shipping.compute_shipping_cost('cart', session[:cart]['tracking_code'])

    options  = {
      :ip         => request.remote_ip,
      :shipping   => shipping,
      :coupon     => session[:cart]['coupon'],
      :website_id => @website.id,
      :order_code => Order.generate_order_code,
      :order_info => params[:order]
    }

    options
  end
end
