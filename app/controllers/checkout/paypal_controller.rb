class Checkout::PaypalController < ApplicationController
  def show
    @cart       = Cart.where tracking_code: params[:id]
    paypal      = Purchase.new 'paypal', @cart, checkout_options
    user, order = paypal.verify

    begin
      @cart.user_id = user.id
      @cart.save
    rescue
    end

    if paypal.success?
      # delete all cart items
      @cart.delete_all
      @uo = UnfinishedOrder.find_by tracking_code: session[:cart]['tracking_code']

      @uo.destroy_workers unless @uo.nil?

      if session[:rebuild]
        # set rebuild date
        @uo.recovered = true
        @uo.recovered_date = Time.now
        @uo.save!

        if session[:rebuild_time_frame]
          # record rejoin in mail tracker
          MailTracker.increment_count("rejoins", session[:rebuild_time_frame])
        end
      else
        @uo.destroy_order unless @uo.nil?
      end

      # stats worker
      UpdateStatisticWorker.perform_async(order.id)

      session.delete(:rebuild_time_frame)
      session.delete(:rebuild)

      # set coupon back to nil
      session[:cart]['coupon'] = nil
      session[:cart]['tracking_code'] = Cart.create_tracking_code

      # sign in user
      sign_in(:user, user)

      redirect_to order_path(order.order_code)
    else
      # check first product to be redirected
      # product = order.order_items.first.product
      product = Product.find @cart.first.product_id

      redirect_to "#{root_path}#{product.slug}?error=true"
    end
  end

  private

  def checkout_options
    shipping = Shipping.compute_shipping_cost('cart', session[:cart]['tracking_code'])

    options  = {
      :ip         => request.remote_ip,
      :token      => params[:token],
      :payer_id   => params[:PayerID],
      :shipping   => shipping,
      :coupon     => session[:cart]['coupon'],
      :website_id => @website.id,
      :order_code => Order.generate_order_code
    }

    options
  end
end
