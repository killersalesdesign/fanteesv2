class UserMailer < ApplicationMailer
  default from: "support@fantees.com.au"
  layout 'mailer'

  def on_success_order(order, password, shipping_cost = 0)
    @password = password
    @order    = order
    @subtotal = 0
    @cut_off  = 0

    @shipping_info = ActiveSupport::JSON.decode(@order.shipping_address)
    # @shipping      = Shipping.compute_shipping_cost('order', order.order_code)
    @shipping      = shipping_cost

    @product  = @order.order_items.first.product

    @delivery_date = @product.end_campaign + 14.days
    if @product.campaign_status == Product::CAMPAIGN[:instock]
      delivery = DeliveryService.new(2, Time.now)
      @delivery_date = delivery.bussiness_days
    end

    mail(:to => @order.buyer.email, :from => "Fantees Family <support@fantees.com.au>", :subject => "[FanTees] Order ID #{@order.order_code}")
  end

  def test_mail
    mail(:to => 'test@test.com', :from => "Fantees Family <support@fantees.com.au>", :subject => "test")
  end

  def unfinished_order_30_minutes(tracking_code)
    @cart = Cart.where("tracking_code = ?", tracking_code)

    if are_products_active(@cart)
      @uo = UnfinishedOrder.find_by_tracking_code tracking_code

      if @uo
        info = eval(@uo.checkout_info)

        unless info["email"].blank?
          @rebuild_url = "#{root_url}cart/#{tracking_code}?time_frame=30m"
          if info["first_name"].blank? or info["first_name"].nil?
            @first_name = "hey"
          else
            @first_name = info["first_name"]
          end
          puts '>>> sending email...'
          mail(:to => info["email"], :from => "Fantees Family <support@fantees.com.au>", :subject => "Your Items Await...")
        end
      end
    end
  end

  def unfinished_order_24_hours(tracking_code)
    @cart = Cart.where("tracking_code = ?", tracking_code)

    if are_products_active(@cart)
      @uo = UnfinishedOrder.find_by_tracking_code tracking_code

      if @uo
        @shipping_cost = Shipping.compute_shipping_cost('cart', tracking_code)
        info = eval(@uo.checkout_info)

        unless info["email"].blank?
          @rebuild_url = "#{root_url}cart/#{tracking_code}?time_frame=1d"
          if info["first_name"].blank? or info["first_name"].nil?
            @first_name = "hey"
          else
            @first_name = info["first_name"]
          end

          mail(:to => info["email"], :from => "Fantees Family <support@fantees.com.au>", :subject => "I've Saved Your Cart [Discount Inside]")
        end
      end
    end
  end

  def unfinished_order_10_days(tracking_code)
    @cart = Cart.where("tracking_code = ?", tracking_code)

    if are_products_active(@cart)
      @uo   = UnfinishedOrder.find_by_tracking_code tracking_code

      if @uo
        info = eval(@uo.checkout_info)

        unless info["email"].blank?
          @rebuild_url = "#{root_url}cart/#{tracking_code}?time_frame=10d"
          if info["first_name"].blank? or info["first_name"].nil?
            @first_name = "hey"
          else
            @first_name = info["first_name"]
          end

          mail(:to => info["email"], :from => "Fantees Family <support@fantees.com.au>", :subject => "New Designs Just Released")
        end
      end
    end
  end

  private

  def are_products_active(cart)
    isActive = false

    unless cart.nil?
      cart.each do |item|
        if item.product.status == Product::STATUS[:approved]
          isActive = true
        end
      end
    end

    isActive
  end

end
