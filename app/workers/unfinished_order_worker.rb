class UnfinishedOrderWorker
  include Sidekiq::Worker

  def perform(mail_type, tracking_code)
  	# case mail_type
  	# when "unfinished_order_30_minutes"
  	# 	UserMailer.unfinished_order_30_minutes(tracking_code).deliver
  	# when "unfinished_order_24_hours"
  	# 	UserMailer.unfinished_order_24_hours(tracking_code).deliver
  	# when "unfinished_order_10_days"
  	# 	UserMailer.unfinished_order_10_days(tracking_code).deliver
  	# end

    UserMailer.send(mail_type, tracking_code).deliver
  end
end