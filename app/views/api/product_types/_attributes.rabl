attributes :id, :international_shipping, :domestic_shipping, :price, :product_id, :category_id, :color_id, :default_view

node do |type|
  {
    :name         => type.category.name,
    :name_color   => type.name_and_color,
    :sizes        => type.order_sizes,
    :quantities   => type.quantities,
    :front_images => {
      thumb:  type.front_image(:thumb),
      medium: type.front_image(:medium),
      original: type.front_image(:original)
    },
    :back_images  => {
      thumb:  type.back_image(:thumb),
      medium: type.back_image(:medium),
      original: type.back_image(:original)
    }
  }
end