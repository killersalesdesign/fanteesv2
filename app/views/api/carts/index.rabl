collection @cart

extends "api/carts/_attributes"

node do |cart|
  {
    :image         => cart.product_type.front_image(:thumb),
    :product_title => cart.product.title,
    :size_name     => cart.size.name,
    :delivery_date => cart.product.end_campaign + 14.days,
    :type_name     => cart.product_type.name_and_color
  }
end