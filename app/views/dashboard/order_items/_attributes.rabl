attributes :id, :quantity, :amount

node do |item|
  {
    title:       item.product.title,
    category:    item.category.name,
    front_image: item.product_type.front_image(:thumb),
    back_image:  item.product_type.back_image(:thumb),
    color:       item.color.name,
    size:        item.size.name
  }
end