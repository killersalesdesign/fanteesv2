collection @orders

extends "dashboard/orders/_attributes"

node do |order|
  {
    title:       order.order_items.first.product.title,
    front_image: order.order_items.first.product_type.front_image(:medium),
    back_image:  order.order_items.first.product_type.back_image(:medium)
  }
end