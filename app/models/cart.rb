class Cart < ActiveRecord::Base
  belongs_to :user
  belongs_to :product
  belongs_to :product_type
  belongs_to :size

  def self.create_cart_items(items, user, cart_tracking_code)
    user_id = user.nil? ? nil : user.id
    items.each do |i|
      item = {
        :user_id         => user_id,
        :product_id      => i[:product_id],
        :size_id         => i[:size_id],
        :product_type_id => i[:product_type_id],
        :price           => i[:price],
        :quantity        => i[:quantity],
        :tracking_code   => cart_tracking_code
      }

      begin
        carts = Cart.where tracking_code: cart_tracking_code, product_id: i[:product_id], product_type: i[:product_type_id], size_id: i[:size_id]
        if carts.count > 0
          carts.first.increment!(:quantity, i[:quantity])
        else
          Cart.create item
        end
      rescue
      end
    end

    Cart.where tracking_code: cart_tracking_code
  end

  def self.create_tracking_code
    code ||= loop do
      random_code = SecureRandom.hex[0..12].upcase
      break random_code unless Cart.exists?(tracking_code: random_code)
    end

    return code
  end

end
