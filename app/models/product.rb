class Product < ActiveRecord::Base

  # statuses
  # usage: Product::STATUS[:pending], Product::STATUS[:approved]
  STATUS = {
    pending:      0,
    approved:     1,
    declined:     2,
    deactivated:  3
  }

  # campaign statuses
  # usage: Product::CAMPAIGN[:campaign], Product::CAMPAIGN[:printing]
  CAMPAIGN = {
    campaign: 0,
    printing: 1,
    instock:  2
  }

  # classifications
  # usage: Product::CLASSIFICATION[:regular], Product::CLASSIFICATION[:bundle]
  # product classification
  CLASSIFICATION = {
    regular:  0,
    bundle:   1,
    giftcard: 2
  }

  extend FriendlyId
  friendly_id :url, use: :slugged

  has_many :product_types
  has_many :order_items

  belongs_to :website
  belongs_to :user

  def sanitized_description
    ActionView::Base.full_sanitizer.sanitize self.description
  end

  def self.search(keyword, classification)
    if keyword.nil?
      products = self.where(status: Product::STATUS[:approved]).where("end_campaign >= ?", Time.now.beginning_of_day)
    else
      dashed_keyword = keyword.gsub(" ","-")
      products = self.where(status: Product::STATUS[:approved]).where("title LIKE ? OR url LIKE ? OR tags LIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{dashed_keyword}%")
    end

    products.where(classification: classification, show_home: true).order("id DESC")
  end

  def total_sold
    begin
      self.order_items.sum(:quantity)
    rescue
      0
    end
  end
end
