class OrderItem < ActiveRecord::Base
	belongs_to :order
	belongs_to :product
	belongs_to :category
	belongs_to :size
	belongs_to :color
  belongs_to :product_type
end
