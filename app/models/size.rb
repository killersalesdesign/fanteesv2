class Size < ActiveRecord::Base
	belongs_to :style
  belongs_to :category

  SIZE_ORDER = ['XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL', '5XL']
end
