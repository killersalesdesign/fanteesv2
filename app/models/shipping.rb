class Shipping < ActiveRecord::Base
  has_many :categories

  def self.compute_shipping_cost(model, code)
    shipping = Shipping.order("cost ASC")
    cost     = shipping.first.cost

    unless code.nil?
      if model == 'cart'
        items = Cart.where tracking_code: code
      elsif model == 'order'
        order = Order.find_by order_code: code
        items = order.order_items
      end

      if items.sum(:quantity) >= shipping.first.less_than
        cost = shipping.last.cost
      else

        begin
          items.each do |item|
            product_type = ProductType.find item.product_type_id
            category     = Category.find product_type.category_id
            shipping     = Shipping.find category.shipping_id

            cost = cost > shipping.cost ? cost : shipping.cost
          end
        rescue
        end

      end
    end

    return cost
  end

end
