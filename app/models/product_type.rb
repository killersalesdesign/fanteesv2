class ProductType < ActiveRecord::Base
  DEFAULT_VIEW = {
    front: 0,
    back: 1
  }

  belongs_to :product
  belongs_to :category
  belongs_to :color

  has_attached_file :front_image, :styles => { :medium => "415x500>", :thumb => "100x100>" }, :default_url => "/assets/thumbnail-default.jpg"
  has_attached_file :back_image, :styles => { :medium => "415x500>", :thumb => "100x100>" }, :default_url => "/assets/thumbnail-default.jpg"

  def name_and_color
    "#{self.category.name} - #{self.color.name}"
  end

  def default_img_view(type = :original)
    if self.default_view == ProductType::DEFAULT_VIEW[:front]
      self.front_image(type)
    else
      self.back_image(type)
    end
  end

  def sizes
    sizes = []
    self.category.sizes.where(:active => true).each do |s|
      sizes << {id: s.id, name: s.name}
    end

    sizes
  end

  def order_sizes
    sizes = []
    list  = self.category.sizes.where(:active => true).order("position ASC")
    list.each do |s|
      sizes << {id: s.id, name: s.name}
    end

    sizes
  end

  # def order_sizes
  #   sizes = []
  #   list  = self.category.sizes.where(:active => true)
  #   # list  = self.category.colors.find_by(color_id: self.color_id).sizes
  #   order = Size::SIZE_ORDER
  #   order.each do |o|
  #     size = list.detect{|s| s.name == o}
  #     sizes << {id: size.id, name: size.name} unless size.nil?
  #   end

  #   if sizes.empty?
  #     list.each do |s|
  #       sizes << {id: s.id, name: s.name}
  #     end
  #   end

  #   sizes
  # end

  def quantities
    qty = []
    (1..25).each do |i|
      qty << i
    end

    qty
  end

  # def cart_dropdown_types
  #   product_types = {
  #     name_color:
  #   }
  # end
end
