class Statistic < ActiveRecord::Base
  belongs_to :product
  belongs_to :product_type
  belongs_to :category
  belongs_to :color
  belongs_to :user
  belongs_to :size
  belongs_to :website

  def self.rake_task(id)
    order  = Order.find id
    seller = order.seller
    order.order_items.each do |item|

      stat = Statistic.where(
        :created_at      => order.created_at.beginning_of_day..order.created_at.end_of_day,
        :website_id      => order.website_id,
        :product_id      => item.product_id,
        :user_id         => seller.id,
        :color_id        => item.color_id,
        :category_id     => item.category_id,
        :product_type_id => item.product_type_id,
        :size_id         => item.size_id
      )

      if stat.empty?
        product_type = ProductType.find item.product_type_id
        stat         = Statistic.new(
          :website_id      => order.website_id,
          :product_id      => item.product_id,
          :user_id         => seller.id,
          :color_id        => item.color_id,
          :category_id     => item.category_id,
          :product_type_id => item.product_type_id,
          :size_id         => item.size_id,
          :sold            => item.quantity,
          :price           => product_type.price,
          :base_price      => product_type.base_price,
          :profit          => product_type.profit,
          :created_at      => order.created_at
        )
        stat.save
      else
        Statistic.update_counters stat.last.id, :sold => item.quantity
      end
    end

  end
end
