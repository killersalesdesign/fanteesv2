class User < ActiveRecord::Base
  ROLE = {
    admin:  1,
    seller: 2,
    buyer:  3
  }

  has_many :products
  has_many :made_orders, :foreign_key => "buyer_id", :class_name => "Order", :dependent => :destroy
  has_many :sold_orders, :foreign_key => "seller_id", :class_name => "Order"

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def is_admin?
    self.role == User::ROLE[:admin]
  end

  def is_seller?
    self.role == User::ROLE[:seller]
  end

  def is_buyer?
    self.role == User::ROLE[:buyer]
  end
end
