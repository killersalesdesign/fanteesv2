class UnfinishedOrder < ActiveRecord::Base
	if Rails.env == "development" or Rails.env == "staging"
	  WORKERS = [
	    {duration: 1.minute, mail: "unfinished_order_30_minutes"},
	    {duration: 2.minutes, mail: "unfinished_order_24_hours"},
	    {duration: 3.minutes, mail: "unfinished_order_10_days"},
	  ]
	else
		WORKERS = [
		  {duration: 30.minutes, mail: "unfinished_order_30_minutes"},
		  {duration: 1.day, mail: "unfinished_order_24_hours"},
		  {duration: 10.days, mail: "unfinished_order_10_days"},
		]
	end

  belongs_to :website

  def destroy_workers
  	begin
  	  jids = eval(self.worker_ids)
  	  r = Sidekiq::ScheduledSet.new
  	  jobs = r.select {|i| jids.include?(i.jid) }
  	  jobs.each(&:delete)
  	rescue
  	end
  end

  def destroy_order
  	self.delete
  end

end
