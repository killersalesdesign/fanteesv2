class Brand < ActiveRecord::Base
  has_many :categories

  # statuses
  # usage: Brand::INACTIVE, Brand::ACTIVE
  INACTIVE 	= 0
  ACTIVE 		= 1
  
end
