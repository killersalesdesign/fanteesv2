class Order < ActiveRecord::Base
	has_many :order_items, :dependent => :destroy

	belongs_to :buyer, :foreign_key => "buyer_id", :class_name => "User"
  belongs_to :seller, :foreign_key => "seller_id", :class_name => "User"
  belongs_to :website
  belongs_to :coupon

	# statuses
	# usage: Order::PENDING, Order::PRINTING
  STATUS = {
    pending:  0,
    printing: 1,
    shipped:  2,
    refunded: 3
  }

  PAYMENT = {
    paypal:     0,
    creditcard: 1
  }

  def cart
    CartCheckout.new(self)
  end

  def send_mail_on_success_order(password = nil)
    cost = Shipping.compute_shipping_cost('order', self.order_code)

    UserMailer.delay.on_success_order(self, password, cost)
    # UserMailer.on_success_order(self, password).deliver
  end

  def self.generate_order_code
    Rails.logger.debug("GENERATE & CHECK IF ORDER CODE EXIST!")
    code = loop do
      random_code = SecureRandom.hex[0..8].upcase
      break random_code unless Order.exists?(order_code: random_code)
    end

    code
  end
end
