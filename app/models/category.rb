class Category < ActiveRecord::Base
  has_many :brands
  has_many :sizes

  # statuses
  # usage: Category::INACTIVE, Category::ACTIVE
  INACTIVE 	= 0
  ACTIVE 		= 1

end
