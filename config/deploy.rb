set :application, 'teecommerce'
set :deploy_user, 'ubuntu'
set :use_sudo, true

# side bug config
set :pty,  false

# setup repo details
set :scm, :git
set :repo_url, 'git@bitbucket.org:killersalesdesign/fanteesv2.git'

# how many old releases do we want to keep
set :keep_releases, 5

# dirs we want symlinking to shared
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

set :bundle_binstubs, nil

# what specs should be run before deployment is allowed to
# continue, see lib/capistrano/tasks/run_tests.cap
set :tests, []

# which config files should be copied by deploy:setup_config
# see documentation in lib/capistrano/tasks/setup_config.cap
# for details of operations
set :config_files, %w{config/application.yml}

# files we want symlinking to specific entries in shared.
set :linked_files, %w{config/application.yml}

# slackistrano config
set :slack_webhook, "https://hooks.slack.com/services/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"
set :slack_icon_emoji, ":rocket:"
set :slack_username, "#{ENV['USER'].capitalize || ENV['USERNAME'].capitalize}"
set :slack_title_starting, "I am deploying"
set :slack_title_finished, "I finished deploying"
set :slack_title_failed, "Damn!! My deploy failed!"
set :slack_msg_starting, -> {"Branch #{fetch :branch} of #{fetch :application} to #{fetch :stage} (#{fetch :rails_env, 'production'})"}
set :slack_msg_finished, -> {"Branch #{fetch :branch} of #{fetch :application} to #{fetch :stage} (#{fetch :rails_env, 'production'})"}
set :slack_msg_failed, -> {"Branch #{fetch :branch} of #{fetch :application} to #{fetch :stage} (#{fetch :rails_env, 'production'})"}

# this:
# http://www.capistranorb.com/documentation/getting-started/flow/
# is worth reading for a quick overview of what tasks are called
# and when for `cap stage deploy`

namespace :deploy do
  namespace :figaro do
    desc "Transfer Figaro's application.yml to shared/config"
    task :upload do
      on roles(:all) do
        upload! "config/application.yml", "#{shared_path}/config/application.yml"
      end
    end
  end

  # before "deploy:check", "deploy:check:make_linked_dirs"
  # before "deploy:check", "sidekiq:monit:config"
  # before "deploy:check", "unicorn:setup_app_config"
  # before "deploy:check", "unicorn:setup_initializer"
  # before "deploy:check", "unicorn:setup_logrotate"
  # before "deploy:check", "nginx:setup"

  before "deploy:check", "figaro:upload"

  after :finishing, 'deploy:cleanup'

  after :deploy, 'unicorn:restart'

  # As of Capistrano 3.1, the `deploy:restart` task is not called
  # automatically.
  after 'deploy:publishing', 'deploy:restart'
end