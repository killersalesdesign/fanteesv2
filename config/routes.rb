Rails.application.routes.draw do
  # You can have the root of your site routed with "root"
  root 'home#index'

  devise_for :users

  # static pages
  post "pages/send_contact_us"
  get "heres-how",            :controller => :pages, :action => :heres_how
  get "contact-us",           :controller => :pages, :action => :contact_us
  get "about-us",             :controller => :pages, :action => :about_us
  get "faq",                  :controller => :pages, :action => :faq
  get "privacy-policy",       :controller => :pages, :action => :privacy
  get "terms-of-service",     :controller => :pages, :action => :terms
  get "shipping",             :controller => :pages, :action => :shipping
  get "returns-exchanges",    :controller => :pages, :action => :returns_exchanges
  get "sizing",               :controller => :pages, :action => :sizing
  get "process",              :controller => :pages, :action => :processing
  get "pinterest-56764.html", :controller => :pages, :action => :pinterest
  get "win",                  :controller => :pages, :action => :win
  get "user-agreement",       :controller => :pages, :action => :user_agreement

  resources :orders, :only => [:index, :show]

  # checkout pages
  resources :cart, :only => [:index, :show] do
    collection do
      get  :checkout
      get  :creditcard
      post :checkout
    end
  end

  namespace :api do
    get 'carts/shipping_cost', :controller => :carts, :action => :shipping_cost
    resources :carts, :only => [:index, :show, :create, :destroy]
    resources :coupons, :only => :show
    resources :unfinished_orders, :only => :create
    resources :products, :only => [:index, :show] do
      resources :product_types, :only => [:index, :show]
      resources :product_type_colors, :only => [:index]
    end
    # resources :product_types, :only => [:index, :show]
  end

  namespace :checkout do
    resources :paypal, :only => [:show]
    resources :eway,   :only => [:create]
  end

  # get "dashboard", :controller => 'dashboard/home', :action => :index
  get "my-orders", :controller => 'dashboard/home', :action => :index
  namespace :dashboard do
    resources :orders, :only => [:index, :show, :update] do
      resources :order_items, :only => [:index, :show]
    end
  end

  get "render_email_pixel/:time_frame", :controller => :mail_tracker, :action => :render_pixel, :as => :render_email_pixel

  require 'sidekiq/web'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == 'admin' && password == 'f@nt33s'
  end
  mount Sidekiq::Web => '/sidekiq'

  get ":slug", :controller => "products", :action => :show
end
