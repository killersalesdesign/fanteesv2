# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.
# role :app, %w{ubuntu@54.153.188.97}
# role :web, %w{ubuntu@54.153.188.97}
# role :db,  %w{ubuntu@54.153.188.97}

set :stage, :staging
set :branch, "staging"

set :ssh_options, {
   keys: %w(~/.ssh/ksd-sydney-staging.pem)
 }

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

set :full_app_name, "#{fetch(:application)}_#{fetch(:stage)}"
# set :server_name, "www.example.com example.com"

server '54.153.188.97', user: 'ubuntu', roles: %w{web app db}, primary: true

set :deploy_to, "/home/#{fetch(:deploy_user)}/apps/#{fetch(:full_app_name)}"

# dont try and infer something as important as environment from
# stage name.
set :rails_env, :staging

# number of unicorn workers, this will be reflected in
# the unicorn.rb and the monit configs
set :unicorn_worker_count, 5

# whether we're using ssl or not, used for building nginx
# config file
set :enable_ssl, false